export default function client({
  url, method, useAccessToken = true, body = null,
}) {
  const apiEndpoint = `http://${process.env.REACT_APP_BACKEND_HOST}:${process.env.REACT_APP_BACKEND_PORT}/${url}`;
  const headers = {};
  if (useAccessToken) {
    headers.Authorization = `Bearer ${localStorage.getItem('access_token')}`;
  }

  const parameters = {
    method,
    headers,
  };
  if (body !== null) {
    parameters.body = body;
    parameters.headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  return fetch(apiEndpoint, parameters)
    .then((response) => response.json().then((data) => {
      if (!response.ok) {
        throw new Error(data.message);
      }
      return data;
    }));
}
