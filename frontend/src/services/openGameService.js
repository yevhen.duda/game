import client from './client';

export const openGamesDownloadService = () => (client({ url: 'api/game/opened', method: 'GET' }));

export const openGameCreateService = (creds) => (
  client({
    url: 'api/game/opened',
    method: 'POST',
    body: [
      `virus=${creds.virus}`,
      `color=${creds.color}`,
      `board_height=${creds.boardHeight}`,
      `board_width=${creds.boardWidth}`,
      `players_number=${creds.playersNumber}`,
    ].join('&'),
  })
);

export const openGameJoinService = (creds) => (
  client({
    url: 'api/game/opened',
    method: 'PUT',
    body: `game_id=${creds.id}&color=${creds.color}&virus=${creds.virus}`,
  })
);
