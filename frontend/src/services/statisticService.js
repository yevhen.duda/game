import client from './client';

const statisticService = (pageIndex) => (client({ url: `api/statistic?page=${pageIndex}`, method: 'GET' }));

export default statisticService;
