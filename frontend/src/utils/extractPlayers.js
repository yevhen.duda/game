function extractPlayers({ players }) {
  const yourName = localStorage.getItem('username');
  const yourProps = players.find(({ name }) => name === yourName);
  return {
    yourProps: yourProps !== undefined ? yourProps : {},
    opponents: players.filter(({ name }) => name !== yourName),
  };
}

export function extractPlayersFromGame(game) {
  const { yourProps, opponents } = extractPlayers(game);
  return {
    ...game,
    yourProps,
    opponents,
  };
}

export function extractPlayersFromGames(games) {
  return games.map((game) => extractPlayersFromGame(game));
}
