import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import SocketProvider from './context/socketContext';
import NotificationProvider from './context/notificationContext';
import rootReducer from './reducers/rootReducer';
import reportWebVitals from './reportWebVitals';
import Root from './Root';
import rootSaga from './sagas/rootSaga';

const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const myEnv = dotenv.config();
dotenvExpand(myEnv);

const sagaMiddleware = createSagaMiddleware();
const createStoreWithMiddleware = applyMiddleware(thunkMiddleware, sagaMiddleware)(createStore);

const store = createStoreWithMiddleware(
  rootReducer,
  // eslint-disable-next-line no-underscore-dangle
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

sagaMiddleware.run(rootSaga);

render(
  <Provider store={store}>
    <NotificationProvider>
      <SocketProvider>
        <BrowserRouter>
          <Root />
        </BrowserRouter>
      </SocketProvider>
    </NotificationProvider>
  </Provider>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
