import { all } from 'redux-saga/effects';
import { loginSaga, logoutSaga, registrationSaga } from './authSaga';
import statisticSaga from './statisticSaga';
import { openGameDownloadSaga, openGameCreateSaga, openGameJoinSaga } from './openGameSaga';

export default function* rootSaga() {
  yield all([
    loginSaga(),
    logoutSaga(),
    registrationSaga(),
    statisticSaga(),
    openGameDownloadSaga(),
    openGameCreateSaga(),
    openGameJoinSaga(),
  ]);
}
