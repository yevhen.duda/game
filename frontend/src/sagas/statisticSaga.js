import {
  call, put, takeLatest,
} from 'redux-saga/effects';
import { STATISTIC_REQUEST } from '../actions/types';
import statisticService from '../services/statisticService';
import { receiveStatisticAction } from '../actions/statistic';
import { addNotificationAction } from '../actions/notifications';

function* userStatistic(creds) {
  try {
    const result = yield call(statisticService, creds.pageIndex);
    yield put(receiveStatisticAction(result));
  } catch (e) {
    yield put(addNotificationAction({ message: e.message }));
  }
}

export default function* statisticSaga() {
  yield takeLatest(STATISTIC_REQUEST, userStatistic);
}
