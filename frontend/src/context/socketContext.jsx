import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { io } from 'socket.io-client';
import {
  receiveGameListDownloadAction,
  receiveGameDownloadAction,
} from '../actions/game';
import { receiveGameListAction } from '../actions/openGame';
import { receiveStatisticAction } from '../actions/statistic';
import { addNotificationAction } from '../actions/notifications';

const SocketContext = React.createContext();

export const useSocket = () => React.useContext(SocketContext);

function CreateSocketValue({ isAuthenticated }) {
  const [socket, setSocket] = useState(null);
  const [connected, setConnected] = useState(true);
  const dispatch = useDispatch();

  const sendEvent = (type, data = {}) => {
    if (socket) {
      socket.emit(type, data);
    }
  };

  const connectToSocket = () => {
    const newSocket = io(
      `http://${process.env.REACT_APP_BACKEND_HOST}:${process.env.REACT_APP_BACKEND_PORT}/`,
    );
    newSocket.on('connect_error', () => {
      setConnected(false);
      newSocket.off('notification');
      newSocket.off('open games downloaded');
      newSocket.off('game downloaded');
      newSocket.off('games downloaded');
      newSocket.off('game statistic download');
    });
    newSocket.on('connect', () => {
      setConnected(true);
      newSocket.emit('connection', { access_token: localStorage.getItem('access_token') });
      newSocket.on('notification', (response) => {
        dispatch(addNotificationAction({
          type: response.type,
          message: response.message,
        }));
      });
      newSocket.on('open games downloaded', (response) => dispatch(receiveGameListAction(response.games)));
      newSocket.on('game downloaded', (response) => dispatch(receiveGameDownloadAction(response.game)));
      newSocket.on('games downloaded', (response) => dispatch(receiveGameListDownloadAction(response.games)));
      newSocket.on('game statistic download', (response) => dispatch(receiveStatisticAction(response.statistic)));
    });
    setSocket(newSocket);
  };

  const disconnectSocket = () => {
    if (socket) {
      socket.disconnect();
    }
    setSocket(null);
  };

  const makeMove = ({ gameId, row, col }) => {
    sendEvent('do_chunk_of_move', { game_id: gameId, row, col });
  };

  const downloadGames = () => sendEvent('download_games');

  const downloadGame = ({ gameId }) => sendEvent('download_game', { game_id: gameId });

  useEffect(() => {
    if (isAuthenticated) {
      connectToSocket();
    } else {
      disconnectSocket();
    }
  }, [isAuthenticated]);

  return {
    makeMove, downloadGames, downloadGame, connected,
  };
}

function SocketProvider({ children, isAuthenticated }) {
  const socketValue = CreateSocketValue({ isAuthenticated });
  return (
    <SocketContext.Provider value={socketValue}>
      {children}
    </SocketContext.Provider>
  );
}

SocketProvider.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(SocketProvider);
