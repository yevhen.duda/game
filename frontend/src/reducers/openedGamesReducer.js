import {
  GAME_LIST_SUCCESS,
  GAME_JOIN_SUCCESS,
  GAME_CREATE_REQUEST,
  GAME_CREATE_SUCCESS,
  GAME_CREATE_FAIL,
  LOGOUT,
} from '../actions/types';
import {
  extractPlayersFromGames,
  extractPlayersFromGame,
} from '../utils/extractPlayers';

export const initialState = {
  isFetchingGameCreate: false,
  games: [],
};

export default function statisticReducer(state = initialState, action) {
  switch (action.type) {
    case GAME_LIST_SUCCESS:
      return {
        ...state,
        games: extractPlayersFromGames(action.games),
      };

    case GAME_JOIN_SUCCESS:
      return {
        ...state,
        game: extractPlayersFromGame(action.game),
      };

    case GAME_CREATE_REQUEST:
      return {
        ...state,
        isFetchingGameCreate: action.isFetchingGameCreate,
      };

    case GAME_CREATE_SUCCESS:
      return {
        ...state,
        isFetchingGameCreate: action.isFetchingGameCreate,
      };

    case GAME_CREATE_FAIL:
      return {
        ...state,
        isFetchingGameCreate: action.isFetchingGameCreate,
      };

    case LOGOUT:
      return {
        ...state,
        games: [],
      };

    default:
      return state;
  }
}
