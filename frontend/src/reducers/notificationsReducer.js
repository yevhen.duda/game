import {
  NOTIFICATION_ADD,
  NOTIFICATION_REMOVE,
} from '../actions/types';

export const initialState = [];

export default function notificationsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case NOTIFICATION_ADD:
      return [payload, ...state];

    case NOTIFICATION_REMOVE:
      return state.filter((notification) => notification.id !== payload.id);

    default:
      return state;
  }
}
