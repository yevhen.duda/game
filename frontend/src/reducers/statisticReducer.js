import {
  STATISTIC_SUCCESS,
  STATISTIC_REQUEST,
  LOGOUT,
} from '../actions/types';
import { extractPlayersFromGames } from '../utils/extractPlayers';

export const initialState = {
  isFetching: false,
  gamesCount: 0,
  pagesCount: 0,
  page: {},
};

export default function statisticReducer(state = initialState, action) {
  switch (action.type) {
    case STATISTIC_REQUEST:
      return {
        ...state,
        isFetching: action.isFetching,
      };

    case STATISTIC_SUCCESS:
      return {
        ...state,
        isFetching: action.isFetching,
        gamesCount: action.gamesCount,
        pagesCount: action.pagesCount,
        page: {
          ...action.page,
          games: extractPlayersFromGames(action.page.games),
        },
      };

    case LOGOUT:
      return {
        ...state,
        gamesCount: 0,
        pagesCount: 0,
        page: {},
      };

    default:
      return state;
  }
}
