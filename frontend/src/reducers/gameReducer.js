import {
  GAMES_DOWNLOAD_SUCCESS,
  GAME_DOWNLOAD_SUCCESS,
  LOGOUT,
  GAME_JOIN_SUCCESS,
  GAME_CREATE_SUCCESS,
} from '../actions/types';
import { extractPlayersFromGames } from '../utils/extractPlayers';

function replaceGame(games, game) {
  return games.map((g) => {
    if (g.id === game.id) {
      return game;
    }
    return g;
  });
}

export const initialState = {
  games: [],
};

export default function gameReducer(state = initialState, action) {
  switch (action.type) {
    case GAMES_DOWNLOAD_SUCCESS:
      return {
        ...state,
        games: extractPlayersFromGames(action.games),
      };

    case GAME_DOWNLOAD_SUCCESS:
      return {
        ...state,
        games: extractPlayersFromGames(replaceGame(state.games, action.game)),
      };

    case GAME_JOIN_SUCCESS:
      return {
        ...state,
        games: extractPlayersFromGames([action.game, ...state.games]),
      };

    case GAME_CREATE_SUCCESS:
      return {
        ...state,
        games: extractPlayersFromGames([action.game, ...state.games]),
      };

    case LOGOUT:
      return {
        ...state,
        games: [],
      };

    default:
      return state;
  }
}
