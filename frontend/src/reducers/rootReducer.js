import { combineReducers } from 'redux';
import authReducer from './authReducer';
import statisticReducer from './statisticReducer';
import openedGamesReducer from './openedGamesReducer';
import gameReducer from './gameReducer';
import notificationsReducer from './notificationsReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  statistic: statisticReducer,
  openGame: openedGamesReducer,
  game: gameReducer,
  notifications: notificationsReducer,
});

export default rootReducer;
