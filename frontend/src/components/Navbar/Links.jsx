import React from 'react';
import {
  Link,
} from 'react-router-dom';

export function RegistrationLink() {
  return (
    <Link to="registration" className="btn btn-lg btn-secondary text-light">
      Registration
    </Link>
  );
}

export function LoginLink() {
  return (
    <Link to="login" className="btn btn-lg btn-success ml-3">Login</Link>
  );
}
