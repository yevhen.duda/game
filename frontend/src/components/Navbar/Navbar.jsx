import React from 'react';
import { connect } from 'react-redux';
import {
  Route,
  Switch,
  Link,
  Redirect,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import { Navbar as BaseNavbar, Container, Nav } from 'react-bootstrap';
import Logout from '../Auth/Logout';
import { LoginLink, RegistrationLink } from './Links';

function Navbar(props) {
  const { dispatch, isAuthenticated } = props;

  return (
    <BaseNavbar bg="primary" expand="sm" style={{ position: 'fixed', width: '100%', zIndex: '100' }}>
      {!isAuthenticated && (
        <Container>
          <div />
          <Switch>
            <Route path="/registration">
              <LoginLink />
            </Route>
            <Route path="/login">
              <RegistrationLink />
            </Route>
            <Route path="/">
              <Redirect to="login" />
            </Route>
          </Switch>
        </Container>
      )}
      {isAuthenticated && (
        <Container>
          <BaseNavbar.Toggle aria-controls="basic-navbar-nav" />
          <BaseNavbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Link
                to="/game_list"
                className="btn text-white font-weight-bolder"
                style={{ marginLeft: '10px' }}
              >
                Games
              </Link>
              <Link
                to="/join"
                className="btn text-white font-weight-bolder"
                style={{ marginLeft: '10px' }}
              >
                Join
              </Link>
              <Link
                to="/create"
                className="btn text-white font-weight-bolder"
                style={{ marginLeft: '10px' }}
              >
                Create
              </Link>
              <Link
                to="/statistic"
                className="btn text-white font-weight-bolder"
                style={{ marginLeft: '10px' }}
              >
                Statistic
              </Link>
            </Nav>
          </BaseNavbar.Collapse>
          <div>
            <Logout dispatch={dispatch} />
          </div>
        </Container>
      )}
    </BaseNavbar>
  );
}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(Navbar);
