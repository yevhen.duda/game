import React from 'react';
import { logoutUserAction } from '../../actions/auth';

function handleSubmit(event, props) {
  event.preventDefault();
  props.dispatch(logoutUserAction());
}

export default function Logout(props) {
  return (
    <form onSubmit={(event) => handleSubmit(event, props)}>
      <input
        type="submit"
        className="form-control btn btn-danger btn-md"
        value="Logout"
      />
    </form>
  );
}
