import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Pagination } from 'react-bootstrap';
import EndedGameInfo from './EndedGameInfo';
import { StatisticPageType } from '../../types';
import { requestStatisticAction } from '../../actions/statistic';

function Statistic(props) {
  const {
    page, isFetching, pagesCount, gamesCount,
  } = props;

  const { games } = page;
  const isActivePrevPageButton = page.page_index !== 1;
  const isActiveNextPageButton = page.page_index !== pagesCount;
  const gamesExist = games && games.length > 0;

  return (
    <div style={{ marginTop: '10px' }}>
      <div>
        <div className="row">
          <div className="col">
            {(gamesExist && pagesCount > 1) && (
              <Pagination>
                <Pagination.Prev
                  onClick={() => {
                    if (isActivePrevPageButton) {
                      props.dispatch(requestStatisticAction(page.page_index - 1));
                    }
                  }}
                  active={isActivePrevPageButton}
                />
                {page.page_index > 2 && (
                  <Pagination.Item
                    onClick={() => props.dispatch(requestStatisticAction(1))}
                  >
                    1
                  </Pagination.Item>
                )}
                {page.page_index > 3 && (
                  <Pagination.Ellipsis />
                )}
                {page.page_index !== 1 && (
                  <Pagination.Item
                    onClick={() => props.dispatch(requestStatisticAction(page.page_index - 1))}
                  >
                    {page.page_index - 1}
                  </Pagination.Item>
                )}
                <Pagination.Item active>{page.page_index}</Pagination.Item>
                {page.page_index !== pagesCount && (
                  <Pagination.Item
                    onClick={() => props.dispatch(requestStatisticAction(page.page_index + 1))}
                  >
                    {page.page_index + 1}
                  </Pagination.Item>
                )}
                {page.page_index < pagesCount - 2 && (
                  <Pagination.Ellipsis />
                )}
                {page.page_index < pagesCount - 1 && (
                  <Pagination.Item
                    onClick={() => props.dispatch(requestStatisticAction(pagesCount))}
                  >
                    {pagesCount}
                  </Pagination.Item>
                )}
                <Pagination.Next
                  onClick={() => {
                    if (isActiveNextPageButton) {
                      props.dispatch(requestStatisticAction(page.page_index + 1));
                    }
                  }}
                  active={isActiveNextPageButton}
                />
              </Pagination>
            )}
          </div>
          <div className="col">
            {isFetching && (
              <div className="spinner-border text-success" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            )}
          </div>
        </div>
        {gamesExist && (
          <>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Opponent</th>
                  <th scope="col">Your Virus</th>
                  <th scope="col">Result</th>
                  <th scope="col">Ended</th>
                </tr>
              </thead>
              <tbody>
                {games.map((game) => (
                  <EndedGameInfo game={game} key={`game_${game.id}`} />
                ))}
              </tbody>
            </table>
            <p style={{ fontWeight: 'bold' }}>{`You have ${gamesCount} completed games`}</p>
          </>
        )}
      </div>
      {!gamesExist && "You don't have ended games"}
    </div>
  );
}

Statistic.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  gamesCount: PropTypes.number.isRequired,
  pagesCount: PropTypes.number.isRequired,
  page: StatisticPageType.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => (state.statistic);

export default connect(mapStateToProps)(Statistic);
