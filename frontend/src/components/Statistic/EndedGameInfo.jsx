import React from 'react';
import Virus from '../Virus/Virus';
import { gameResultType } from '../../types';

function dateToString(date) {
  function addLeadingZero(value) {
    return (value > 9 ? '' : '0') + value;
  }

  return [
    [
      addLeadingZero(date.getMonth() + 1),
      addLeadingZero(date.getDate()),
      date.getFullYear(),
    ].join('.'),
    [
      addLeadingZero(date.getHours()),
      addLeadingZero(date.getMinutes()),
    ].join(':'),
  ].join(' ');
}

export default function EndedGameInfo({ game }) {
  const { yourProps, opponents } = game;
  const endGame = game.datetime_end_game !== '' ? dateToString(new Date(game.datetime_end_game)) : '';

  return (
    <tr>
      <td>{opponents.map((op) => op.name).join(', ')}</td>
      <td><Virus type={yourProps.virus} /></td>
      <td>{yourProps.result}</td>
      <td>{endGame}</td>
    </tr>
  );
}

EndedGameInfo.propTypes = {
  game: gameResultType.isRequired,
};
