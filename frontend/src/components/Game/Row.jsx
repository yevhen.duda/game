import React from 'react';
import PropTypes from 'prop-types';
import Cell from './Cell';
import { rowType, playerShortType } from '../../types';

export default function Row(props) {
  const {
    gameId, yourProps, opponents, cells, isYourTurn,
  } = props;

  return (
    <div className="row">
      {cells.map((cell) => (
        <Cell
          gameId={gameId}
          cell={cell}
          isYourTurn={isYourTurn}
          yourProps={yourProps}
          opponents={opponents}
          key={cell.row + cell.col}
        />
      ))}
    </div>
  );
}

Row.propTypes = {
  gameId: PropTypes.number.isRequired,
  cells: rowType.isRequired,
  yourProps: playerShortType.isRequired,
  opponents: PropTypes.arrayOf(playerShortType).isRequired,
  isYourTurn: PropTypes.bool.isRequired,
};
