import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

export default function Timer(props) {
  const { dateTimeLimit, isYourTurn } = props;

  const [timeDelta, setTimeDelta] = useState();

  useEffect(() => {
    const interval = setInterval(() => {
      setTimeDelta(Math.round((new Date(dateTimeLimit) - new Date()) / 1000));
    }, 1000);
    return () => clearInterval(interval);
  });

  return (
    <div>
      <span style={{ marginLeft: '30px' }}>
        {timeDelta > 0 && (
          (isYourTurn && `You have ${timeDelta} seconds`)
          || `Opponent have ${timeDelta} seconds`
        )}
      </span>
    </div>
  );
}

Timer.propTypes = {
  dateTimeLimit: PropTypes.string.isRequired,
  isYourTurn: PropTypes.bool.isRequired,
};
