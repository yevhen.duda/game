import React from 'react';
import { connect } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Tabs, Tab } from 'react-bootstrap';
import { gameType } from '../../types';
import Game from './Game';
import Virus from '../Virus/Virus';
import './TabTitle.css';

function TabTitle(props) {
  const { game } = props;
  const { yourProps, opponents } = game;
  const isActiveGame = game.is_active;

  return (
    <div
      className={isActiveGame ? 'active-game' : 'no-active-game'}
    >
      {(yourProps.result !== '' && yourProps.result)
        || (isActiveGame && <Virus type={yourProps.virus} size="1.5em" colorVirus={yourProps.color || 'black'} />)
        || `${opponents.length + 1}/${game.players_count}`}
      <span style={{
        position: 'absolute',
        color: 'green',
        marginLeft: '5px',
        transform: 'translateY(-1.7em) translateX(0.3em)',
      }}
      >
        {isActiveGame && (
          (yourProps.remaining_move_chunks > 0 && <sup>&#9679;</sup>) || <sup>&emsp;</sup>
        )}
      </span>
    </div>
  );
}

TabTitle.propTypes = {
  game: gameType.isRequired,
};

function GamePage(props) {
  const { games } = props;
  const sortedGames = games.sort(
    (first, second) => !first.is_active && second.is_active,
  );

  let { gameId } = useParams();
  gameId = parseInt(gameId, 10);

  const history = useHistory();

  return (
    <>
      <Tabs
        defaultActiveKey={gameId}
        onSelect={(k) => { history.push(`/game/${k}`); }}
      >
        {sortedGames.map((game) => (
          <Tab
            eventKey={game.id}
            title={<TabTitle game={game} />}
            key={`tab_${game.id}`}
          >
            {game.id === gameId && (<Game game={game} />)}
          </Tab>
        ))}
      </Tabs>
    </>
  );
}

GamePage.propTypes = {
  games: PropTypes.arrayOf(gameType).isRequired,
};

const mapStateToProps = (state) => ({
  games: state.game.games,
});

export default connect(mapStateToProps)(GamePage);
