import React from 'react';
import { useSelector } from 'react-redux';
import { Toast } from 'react-bootstrap';
import { useNotifications } from '../context/notificationContext';

export default function Notifications() {
  const notifications = useSelector((state) => state.notifications);
  const { removeNotification } = useNotifications();

  return (
    <div
      position="bottom-end"
      style={{
        position: 'fixed',
        bottom: '0',
        right: '0',
        zIndex: 1000,
      }}
    >
      {
        notifications.map(({
          type, id, message, background,
        }) => (
          <Toast className="d-inline-block m-1" bg={background} key={id} onClose={() => removeNotification(id)}>
            <Toast.Header>
              <strong className="me-auto">{type[0].toUpperCase() + type.substring(1)}</strong>
            </Toast.Header>
            <Toast.Body>
              {message}
            </Toast.Body>
          </Toast>
        ))
      }
    </div>
  );
}
