import React from 'react';
import PropTypes from 'prop-types';

export default function AliveCircleVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
        borderStyle: 'solid',
        borderWidth: `${0.2 * size}${sizeType}`,
        borderColor: color,
        borderRadius: '50%',
      }}
    >
      &nbsp;
    </div>
  );
}

AliveCircleVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

AliveCircleVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
