import React from 'react';
import PropTypes from 'prop-types';

export default function RhombusVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
        backgroundColor: color,
        transform: 'scale(0.6, 0.8) rotate(45deg)',
      }}
    >
      &nbsp;
    </div>
  );
}

RhombusVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

RhombusVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
