import React from 'react';
import PropTypes from 'prop-types';

export default function CoronaVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  const triangleSize = 0.4 * size;
  const defaultOffsetX = 0.5 * size - triangleSize;
  const defaultOffsetY = 0.6 * (size - triangleSize);

  const upTriangleOffset = triangleSize;
  const offsetX = 0.95 * triangleSize;
  const offsetY = 0.6 * triangleSize;

  const lineStyle = {
    width: '0px',
    height: '0px',
    position: 'relative',
    borderLeft: `${triangleSize}${sizeType} solid transparent`,
    borderRight: `${triangleSize}${sizeType} solid transparent`,
    borderBottom: `${triangleSize}${sizeType} solid ${color}`,
    margin: '0 0 0 0',
  };

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
      }}
    >
      <div
        className={className}
        style={{
          ...lineStyle,
          transform: [
            `translateX(${defaultOffsetX}${sizeType})`,
            `translateY(${defaultOffsetY - upTriangleOffset}${sizeType})`,
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...lineStyle,
          transform: [
            `translateX(${defaultOffsetX + offsetX}${sizeType})`,
            `translateY(${defaultOffsetY - triangleSize + offsetY}${sizeType})`,
            'rotate(120deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...lineStyle,
          transform: [
            `translateX(${defaultOffsetX - offsetX}${sizeType})`,
            `translateY(${defaultOffsetY - 2 * triangleSize + offsetY}${sizeType})`,
            'rotate(-120deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
    </div>
  );
}

CoronaVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

CoronaVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
