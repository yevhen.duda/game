import React from 'react';
import PropTypes from 'prop-types';

export default function CrossVirus(props) {
  const {
    size, sizeType, color, className,
  } = props;

  const wFactor = 1.1;
  const hFactor = 0.2;

  const xTranslate = -0.5 * (wFactor - 1) * size;
  const yTranslate = 0.5 * (1 - hFactor) * size;

  const pointSize = hFactor * size;
  const pointDefaultOffset = -2 * hFactor * size;
  const pointOffsetX = 0.5 * (size - pointSize);
  const pointOffsetY = 0.5 * (size - pointSize) + pointDefaultOffset;
  const pointBigOffsetX = size - pointSize;
  const pointBigOffsetY = size - pointSize + pointDefaultOffset;

  const lineStyle = {
    position: 'relative',
    width: `${wFactor * size}${sizeType}`,
    height: `${hFactor * size}${sizeType}`,
    margin: '0 0 0 0',
    backgroundColor: color,
  };

  const pointStyle = {
    position: 'relative',
    width: `${pointSize}${sizeType}`,
    height: `${pointSize}${sizeType}`,
    margin: '0 0 0 0',
    backgroundColor: color,
  };

  return (
    <div
      className={className}
      style={{
        width: `${size}${sizeType}`,
        height: `${size}${sizeType}`,
      }}
    >
      <div
        className={className}
        style={{
          ...lineStyle,
          transform: [
            `translateX(${xTranslate}${sizeType})`,
            `translateY(${yTranslate}${sizeType})`,
            'rotate(45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...lineStyle,
          transform: [
            `translateX(${xTranslate}${sizeType})`,
            `translateY(${yTranslate - hFactor * size}${sizeType})`,
            'rotate(-45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...pointStyle,
          transform: `translateY(${pointOffsetY}${sizeType}) rotate(45deg)`,
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...pointStyle,
          transform: [
            `translateX(${pointOffsetX}${sizeType})`,
            `translateY(${pointDefaultOffset - pointSize}${sizeType})`,
            'rotate(45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...pointStyle,
          transform: [
            `translateX(${pointBigOffsetX}${sizeType})`,
            `translateY(${pointOffsetY - 2 * pointSize}${sizeType})`,
            'rotate(45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
      <div
        className={className}
        style={{
          ...pointStyle,
          transform: [
            `translateX(${pointOffsetX}${sizeType})`,
            `translateY(${pointBigOffsetY - 3 * pointSize}${sizeType})`,
            'rotate(45deg)',
          ].join(' '),
        }}
      >
        &nbsp;
      </div>
    </div>
  );
}

CrossVirus.defaultProps = {
  sizeType: 'px',
  color: 'black',
  className: '',
};

CrossVirus.propTypes = {
  size: PropTypes.number.isRequired,
  sizeType: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};
