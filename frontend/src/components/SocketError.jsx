import React from 'react';

export default function SocketError() {
  return (
    <div style={{
      position: 'fixed',
      top: '54px',
      width: '100%',
      zIndex: '100',
      backgroundColor: '#f28e53',
      textAlign: 'center',
      paddingBottom: '3px',
      fontWeight: 'bold',
    }}
    >
      Server connection lost. We are trying to restore connection with the server.
    </div>
  );
}
