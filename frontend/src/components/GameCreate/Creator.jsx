import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ColorSelector from './Selectors/ColorSelector';
import VirusSelector from './Selectors/VirusSelector';
import { requestCreateGameAction } from '../../actions/openGame';

function handleSubmit(event, props) {
  event.preventDefault();
  const virus = event.target.virus.value.trim();
  const color = event.target.color.value.trim();
  const boardHeight = event.target.board_height.value.trim();
  const boardWidth = event.target.board_width.value.trim();
  const playersNumber = event.target.players_number.value.trim();
  props.dispatch(requestCreateGameAction({
    virus, color, boardHeight, boardWidth, playersNumber,
  }));
}

function NumberInput({
  name, min, max, defaultValue,
}) {
  return (
    <input
      type="number"
      name={name}
      min={min}
      max={max}
      defaultValue={defaultValue}
      className="form-control"
      style={{ maxWidth: '300px' }}
    />
  );
}

NumberInput.propTypes = {
  name: PropTypes.string.isRequired,
  min: PropTypes.number,
  max: PropTypes.number,
  defaultValue: PropTypes.number,
};

NumberInput.defaultProps = {
  min: 4,
  max: 30,
  defaultValue: 10,
};

function Creator(props) {
  const { isFetching } = props;

  return (
    <div style={{ marginTop: '10px' }}>
      {!isFetching && (
        <form onSubmit={(event) => handleSubmit(event, props)}>
          <input
            type="submit"
            value="Create new game"
            className="btn btn-success"
          />
          <ColorSelector name="color" />
          <VirusSelector name="virus" />
          <br />
          <p>
            <span>Board height: </span>
            <NumberInput name="board_height" />
          </p>
          <p>
            <span>Board width: </span>
            <NumberInput name="board_width" />
          </p>
          <p>
            <span>Number of players: </span>
            <NumberInput name="players_number" min={2} max={4} defaultValue={2} />
          </p>
        </form>
      )}
      {isFetching && (
        <div className="spinner-border text-success" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      )}
    </div>
  );
}

Creator.propTypes = {
  isFetching: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isFetching: state.openGame.isFetchingGameCreate,
});

export default connect(mapStateToProps)(Creator);
