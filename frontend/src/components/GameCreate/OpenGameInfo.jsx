import React from 'react';
import { connect } from 'react-redux';
import ColorSelector from './Selectors/ColorSelector';
import VirusSelector from './Selectors/VirusSelector';
import Virus from '../Virus/Virus';
import { openedGameType } from '../../types';
import { requestJoinGameAction } from '../../actions/openGame';
import './openGameInfo.css';

function handleJoin(event, props) {
  event.preventDefault();
  const { id } = props.game;
  const virus = event.target.virus.value.trim();
  const color = event.target.color.value.trim();
  props.dispatch(requestJoinGameAction({ id, virus, color }));
}

function GameInfo(props) {
  const { game } = props;
  const { opponents } = game;

  return (
    <>
      <tr>
        <td>
          {opponents.map((opponent) => (
            <p
              style={{ fontSize: 'large' }}
              key={`opponent_${opponent.name}`}
            >
              {opponent.name}
            </p>
          ))}
        </td>
        <td>
          {`${game.opponents.length}/${game.players_count}`}
        </td>
        <td>
          {opponents.map((opponent) => (
            <Virus
              type={opponent.virus}
              colorVirus={opponent.color}
              key={`virus_of_${opponent.name}`}
              className="open-game-virus"
            />
          ))}
        </td>
        <td>{`${game.board_height}x${game.board_width}`}</td>
      </tr>
      <tr style={{ borderBottom: ' solid black 2px' }}>
        <td colSpan="4">
          <form onSubmit={(event) => handleJoin(event, props)}>
            <div className="row">
              <div className="col side" />
              <div className="col">
                <ColorSelector name="color" bannedColors={opponents.map((op) => op.color)} />
              </div>
              <div className="col">
                <VirusSelector name="virus" bannedViruses={opponents.map((op) => op.virus)} />
              </div>
              <div className="col">
                <input type="submit" value="Join" className="btn btn-info" />
              </div>
              <div className="col side" />
            </div>
          </form>
        </td>
      </tr>
    </>
  );
}

GameInfo.propTypes = {
  game: openedGameType.isRequired,
};

const mapStateToProps = (state) => ({
  dispatch: state.dispatch,
});

export default connect(mapStateToProps)(GameInfo);
