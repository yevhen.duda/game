import React from 'react';
import PropTypes from 'prop-types';
import Selector from './Selector';
import { virusColors } from '../../Virus/Virus';
import './ColorSelector.css';

export default function ColorSelector({ name, bannedColors }) {
  const colorList = ['black', 'red', 'orange', 'green', 'purple'];

  return (
    <Selector
      name={name}
      options={colorList}
      bannedOptions={bannedColors}
      childrenFactory={({ option, selected }) => (option === selected ? '🗸' : ' ')}
      defaultClass="color-check_btn"
      stileFactory={(color) => ({ backgroundColor: virusColors[color], color: 'white' })}
    />
  );
}

ColorSelector.propTypes = {
  name: PropTypes.string.isRequired,
  bannedColors: PropTypes.arrayOf(PropTypes.string),
};

ColorSelector.defaultProps = {
  bannedColors: [],
};
