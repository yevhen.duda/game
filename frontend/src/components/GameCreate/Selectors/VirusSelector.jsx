import React from 'react';
import PropTypes from 'prop-types';
import Selector from './Selector';
import Virus from '../../Virus/Virus';
import './VirusSelector.css';

export default function VirusSelector({ name, bannedViruses }) {
  const virusList = ['0', '1', '2', '3'];

  return (
    <Selector
      name={name}
      options={virusList}
      bannedOptions={bannedViruses}
      childrenFactory={({ option }) => (<Virus type={option} size="30px" colorExact="white" />)}
      defaultClass="virus-check_btn"
    />
  );
}

VirusSelector.propTypes = {
  name: PropTypes.string.isRequired,
  bannedViruses: PropTypes.arrayOf(PropTypes.string),
};

VirusSelector.defaultProps = {
  bannedViruses: [],
};
