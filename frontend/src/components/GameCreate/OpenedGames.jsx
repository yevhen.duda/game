import React from 'react';
import { connect } from 'react-redux';
import OpenGameInfo from './OpenGameInfo';
import { openedGamesType } from '../../types';

function OpenedGames(props) {
  const { games } = props;
  const openedGames = games.filter((item) => Object.keys(item.yourProps).length === 0);

  return (
    <div style={{ marginTop: '10px' }}>
      {(openedGames.length > 0 && (
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Opponents</th>
              <th scope="col">Number of players</th>
              <th scope="col">Viruses</th>
              <th scope="col">Board size</th>
            </tr>
          </thead>
          <tbody>
            {openedGames.map((game) => (
              <OpenGameInfo
                game={game}
                key={`open_game_${game.id}`}
              />
            ))}
          </tbody>
        </table>
      )) || 'No games to join'}
    </div>
  );
}

OpenedGames.propTypes = {
  games: openedGamesType.isRequired,
};

const mapStateToProps = (state) => ({
  games: state.openGame.games,
});

export default connect(mapStateToProps)(OpenedGames);
