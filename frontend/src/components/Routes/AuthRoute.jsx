import React from 'react';
import { connect } from 'react-redux';
import {
  Route,
  Redirect,
} from 'react-router-dom';
import PropTypes from 'prop-types';

function AuthRoute(props) {
  const {
    path, component, isAuthenticated, redirectNotAuth,
  } = props;

  if (!isAuthenticated && redirectNotAuth) {
    return (
      <Route>
        <Redirect to="/login" />
      </Route>
    );
  }
  if (isAuthenticated && !redirectNotAuth) {
    return (
      <Route>
        <Redirect to="/games" />
      </Route>
    );
  }
  return (
    <Route {...{ path, component }} />
  );
}

AuthRoute.defaultProps = {
  redirectNotAuth: true,
};

AuthRoute.propTypes = {
  path: PropTypes.string.isRequired,
  component: PropTypes.elementType.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  redirectNotAuth: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(AuthRoute);
