import PropTypes from 'prop-types';

export const cellType = PropTypes.shape({
  row: PropTypes.number.isRequired,
  col: PropTypes.number.isRequired,
  virus: PropTypes.string.isRequired,
  is_captured: PropTypes.bool.isRequired,
});

export const rowType = PropTypes.arrayOf(cellType);

export const boardType = PropTypes.arrayOf(rowType);

export const playerShortType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  virus: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
});

const playerType = PropTypes.shape({
  ...playerShortType,
  remaining_move_chunks: PropTypes.number.isRequired,
  result: PropTypes.string.isRequired,
});

export const gameType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  players_count: PropTypes.number.isRequired,
  yourProps: PropTypes.oneOfType([playerType, playerShortType]).isRequired,
  opponents: PropTypes.arrayOf(PropTypes.oneOfType([playerType, playerShortType])).isRequired,
  can_pass_move: PropTypes.bool.isRequired,
  datetime_limit_for_move: PropTypes.string.isRequired,
  board: boardType.isRequired,
  is_ended: PropTypes.bool.isRequired,
  is_active: PropTypes.bool.isRequired,
});

export const gamesType = PropTypes.arrayOf(gameType);

export const gameResultType = PropTypes.shape({
  id: PropTypes.number,
  yourProps: playerShortType.isRequired,
  opponents: PropTypes.arrayOf(playerShortType).isRequired,
  datetime_end_game: PropTypes.string.isRequired,
});

export const StatisticPageType = PropTypes.shape({
  games_count: PropTypes.number,
  page_index: PropTypes.number,
  games: PropTypes.arrayOf(gameResultType),
});

export const statisticGamesType = PropTypes.arrayOf(gameResultType);

export const openedGameType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  players_count: PropTypes.number.isRequired,
  yourProps: PropTypes.oneOfType([playerShortType, PropTypes.shape({})]).isRequired,
  opponents: PropTypes.arrayOf(playerShortType).isRequired,
  board_height: PropTypes.number.isRequired,
  board_width: PropTypes.number.isRequired,
});

export const openedGamesType = PropTypes.arrayOf(openedGameType);
