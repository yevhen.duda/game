import React from 'react';
import { render } from '@testing-library/react';
import EndedGameInfo from '../../../components/Statistic/EndedGameInfo';

test('games count testing', () => {
  const date = (new Date()).toString();
  const name = (Math.random() + 1).toString(36).substring(7);
  const game = {
    yourProps: {
      name: 'i',
      color: 'black',
      virus: '0',
      result: 'draw',
      remaining_move_chunks: 3,
    },
    opponents: [{
      name,
      color: 'orange',
      virus: '1',
      result: 'draw',
      remaining_move_chunks: 0,
    }],
    datetime_end_game: date,
  };

  const { getByText } = render(<table><tbody><EndedGameInfo game={game} /></tbody></table>);
  expect(getByText(name)).toBeInTheDocument();
  expect(getByText('draw')).toBeInTheDocument();
});
