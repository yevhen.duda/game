import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import Statistic from '../../../components/Statistic/Statistic';
import initialStateCreate from '../initialStateCreate';

const middlewares = [];
const mockStore = configureMockStore(middlewares);

test('loading a statistic', () => {
  const initialState = initialStateCreate({ statistic: { isFetching: true } });
  const store = mockStore(initialState);

  const { container } = render(<Provider store={store}><Statistic /></Provider>);
  expect(container.querySelector('.spinner-border')).toBeInTheDocument();
});
