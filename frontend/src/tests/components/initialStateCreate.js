import { initialState as authInitialState } from '../../reducers/authReducer';
import { initialState as gameInitialState } from '../../reducers/gameReducer';
import { initialState as openedGamesInitialState } from '../../reducers/openedGamesReducer';
import { initialState as statisticInitialState } from '../../reducers/statisticReducer';

export default function initialStateCreate(
  props = {
    auth: {},
    game: {},
    openGame: {},
    statistic: {},
  },
) {
  const state = {
    auth: {
      ...authInitialState,
      ...props.auth,
    },
    game: {
      ...gameInitialState,
      ...props.game,
    },
    openGame: {
      ...openedGamesInitialState,
      ...props.openGame,
    },
    statistic: {
      ...statisticInitialState,
      ...props.statistic,
    },
  };
  return state;
}
