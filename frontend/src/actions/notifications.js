import {
  NOTIFICATION_ADD,
  NOTIFICATION_REMOVE,
} from './types';

export function removeNotificationAction(id) {
  return {
    type: NOTIFICATION_REMOVE,
    payload: {
      id,
    },
  };
}

const notificationActionCreator = (payload) => ({
  type: NOTIFICATION_ADD,
  payload,
});

export const addNotificationAction = ({ type = 'error', message, timeout = 4000 }) => (dispatch) => {
  const notificationID = `message-id${Math.random()}`;
  const backgrounds = { error: 'danger', success: 'success' };
  const notification = {
    id: notificationID,
    type,
    message,
    background: backgrounds[type] || backgrounds.error,
  };
  dispatch(notificationActionCreator(notification));
  setTimeout(() => dispatch(removeNotificationAction(notificationID)), timeout);
};
