import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  AUTH_FAIL,
} from './types';

export function requestLoginAction(creds) {
  return {
    type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    username: creds.username,
    password: creds.password,
  };
}

export function receiveLoginAction(user) {
  return {
    type: LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    access_token: user.access_token,
  };
}

export function logoutUserAction() {
  return {
    type: LOGOUT,
    isFetching: false,
    isAuthenticated: false,
  };
}

export function requestRegisterationAction(creds) {
  return {
    type: REGISTER_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    username: creds.username,
    password: creds.password,
  };
}

export function receiveRegistrationAction(user) {
  return {
    type: REGISTER_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    access_token: user.access_token,
  };
}

export function authErrorAction() {
  return {
    type: AUTH_FAIL,
    isFetching: false,
    isAuthenticated: false,
  };
}
