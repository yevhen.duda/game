import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
} from 'react-router-dom';
import PropTypes from 'prop-types';
import Navbar from './components/Navbar/Navbar';
import Login from './components/Auth/Login';
import Registration from './components/Auth/Registration';
import Statistic from './components/Statistic/Statistic';
import GameList from './components/Games/GameList';
import GamePage from './components/Game/GamePage';
import OpenedGames from './components/GameCreate/OpenedGames';
import Creator from './components/GameCreate/Creator';
import Notifications from './components/Notifications';
import { requestGameListAction } from './actions/openGame';
import AuthRoute from './components/Routes/AuthRoute';
import { useSocket } from './context/socketContext';
import SocketError from './components/SocketError';

function App(props) {
  const { dispatch, isAuthenticated } = props;

  useEffect(() => {
    if (isAuthenticated) {
      dispatch(requestGameListAction());
    }
  }, [isAuthenticated]);

  const { connected } = useSocket();

  return (
    <Router>
      <Navbar dispatch={dispatch} />
      {isAuthenticated && !connected && <SocketError />}
      <div className="container" style={{ paddingTop: `${connected ? 60 : 80}px` }}>
        <Notifications />
        <Switch>
          <AuthRoute path="/login" component={Login} redirectNotAuth={false} />
          <AuthRoute path="/registration" component={Registration} redirectNotAuth={false} />
          <AuthRoute path="/statistic" component={Statistic} />
          <AuthRoute path="/join" component={OpenedGames} />
          <AuthRoute path="/create" component={Creator} />
          <AuthRoute path="/games" component={GameList} />
          <AuthRoute path={'/game/:gameId(\\d+)'} component={GamePage} />
          <AuthRoute path="/" component={() => <Redirect to="/games" />} />
        </Switch>
      </div>
    </Router>
  );
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(App);
