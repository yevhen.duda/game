from enum import Enum, unique


@unique
class Virus(Enum):
    TYPE0 = 0
    TYPE1 = 1
    TYPE2 = 2
    TYPE3 = 3


@unique
class NotificationType(str, Enum):
    ERROR = 'error'
    SUCCESS = 'success'


@unique
class SocketEvent(str, Enum):
    NOTIFICATION = 'notification'
    DOWNLOAD_GAME = 'game downloaded'
    DOWNLOAD_GAMES = 'games downloaded'
    DOWNLOAD_OPEN_GAMES = 'open games downloaded'
    DOWNLOAD_GAME_STATISTIC = 'game statistic download'
