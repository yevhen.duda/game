"""add colors to viruses

Revision ID: ea493a3c3fe4
Revises: 0e3fddd0332c
Create Date: 2021-09-08 16:27:52.329559

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ea493a3c3fe4'
down_revision = '0e3fddd0332c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        'game',
        sa.Column(
            'first_user_color',
            sa.String(length=10),
            server_default='black',
            nullable=False
        )
    )
    op.create_check_constraint(
        'game_first_user_color_check',
        'game',
        "(first_user_color = 'black') OR (first_user_color = 'red') OR "
        "(first_user_color = 'orange') OR (first_user_color = 'green') OR "
        "(first_user_color = 'purple')"
    )
    op.add_column(
        'game',
        sa.Column(
            'second_user_color',
            sa.String(length=10),
            server_default='black',
            nullable=False
        )
    )
    op.create_check_constraint(
        'game_second_user_color_check',
        'game',
        "(second_user_color = 'black') OR (second_user_color = 'red') OR "
        "(second_user_color = 'orange') OR (second_user_color = 'green') OR "
        "(second_user_color = 'purple')"
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('game', 'second_user_color')
    op.drop_column('game', 'first_user_color')
    # ### end Alembic commands ###
