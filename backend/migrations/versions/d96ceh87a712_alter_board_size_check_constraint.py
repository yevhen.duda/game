"""alter_board_size_check_constraint

Revision ID: d96ceh87a712
Revises: f95ceb87a711
Create Date: 2021-09-09 13:08:17.571535

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = 'd96ceh87a712'
down_revision = 'f95ceb87a711'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_constraint(
        'board_size_check',
        'board',
        'check'
    )
    op.create_check_constraint(
        'board_size_check',
        'board',
        'size BETWEEN 4 AND 30'
    )


def downgrade():
    op.drop_constraint(
        'board_size_check',
        'board',
        'check'
    )
    op.create_check_constraint(
        'board_size_check',
        'board',
        'size BETWEEN 4 AND 29'
    )
