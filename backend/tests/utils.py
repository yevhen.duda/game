import random
import string
import unittest
from typing import Dict, List, Optional, Tuple

from backend.app import app
from backend.enums import Virus
from backend.models import BoardMixin, Cell, Game, Player, User, db


class MockBoard(BoardMixin):
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self._cells = []
        for row in range(self.height):
            for col in range(self.width):
                self._cells.append(Cell(row_index=row, col_index=col))

    def size(self):
        return (self.height, self.width,)

    def get_cell(self, row: int, col: int) -> Cell:
        if not (0 <= row < self.height and 0 <= col < self.width):
            raise ValueError('Incorrect row or column index')
        return self._cells[row*self.width + col]

    def _get_start_positions(self):
        return {
            Virus.TYPE0: self.get_cell(0, 0),
            Virus.TYPE1: self.get_cell(self.height-1, self.width-1),
            Virus.TYPE2: self.get_cell(self.height-1, 0),
            Virus.TYPE3: self.get_cell(0, self.width-1),
        }


cell_contents: Dict[str, Tuple[Optional[Virus], bool]] = {
    '_': (None, False),
    'o': (Virus.TYPE0, False,),
    '●': (Virus.TYPE0, True,),
    'x': (Virus.TYPE1, False,),
    '☒': (Virus.TYPE1, True,),
    '+': (Virus.TYPE2, False,),
    '*': (Virus.TYPE2, True,),
    '/': (Virus.TYPE3, False,),
    '%': (Virus.TYPE3, True,),
}


def create_player(
    id, virus, color='black', move_chunks=0, is_passed=False, result=None
):
    return Player(
        id=id,
        virus=virus,
        color=color,
        remaining_move_chunks=move_chunks,
        is_previous_move_passed=is_passed,
        result=result
    )


mock_players = [
    create_player(0, Virus.TYPE0),
    create_player(1, Virus.TYPE1),
    create_player(2, Virus.TYPE2),
    create_player(3, Virus.TYPE3),
]


def copy_to_board(
    cells: List[List[str]], board, players: List[Player] = mock_players
):
    """
    :param cells: list of lists 10x10. List of rows numbering from 10 to 1.
    """
    row_count = len(cells)
    col_count = len(cells[0])
    for i in range(row_count):
        for j in range(col_count):
            content = cell_contents[cells[i][j]]
            owner = None
            for pl in players:
                if pl.virus == content[0]:
                    owner = pl
                    break
            board._cells[(row_count-1-i)*col_count + j].owner = owner
            board._cells[(row_count-1-i)*col_count + j].is_captured =\
                content[1]


def create_board(cells: List[List[str]]):
    width = len(cells[0])
    height = len(cells)
    board = MockBoard(height, width)
    copy_to_board(cells, board)
    return board


def empty_board(row_count: int = 10, col_count: int = 10):
    return create_board([['_']*col_count]*row_count)


def users_create(users_number: int = 2) -> List[User]:
    users = []
    for _ in range(users_number):
        user_not_exist = False
        name = ''
        while not user_not_exist:
            name = ''.join(random.choices(string.ascii_letters, k=10))
            user = User.query.filter_by(username=name).one_or_none()
            user_not_exist = user is None
        new_user = User(username=name, password=name)
        db.session.add(new_user)
        users.append(new_user)
    return users


viruses = list(Virus)


def game_create(
    row_count: int = 10, col_count: int = 10, players_number: int = 2
) -> Game:
    users = users_create(players_number)
    game = Game(row_count, col_count, total_players_number=players_number)
    db.session.add(game)
    db.session.commit()
    for i, user in enumerate(users):
        game.join_user(user, viruses[i], Player.VIRUS_COLORS[i], False)
    db.session.add(game)
    db.session.commit()
    return game


def set_board(cells: List[List[str]], game: Game):
    copy_to_board(cells, game, game.players)
    db.session.commit()


def game_remove(game):
    users = [player.user for player in game.players]
    db.session.delete(game)
    for user in users:
        db.session.delete(user)
    db.session.commit()


def set_active_player(game: Game, virus_str: str = 'x'):
    virus = cell_contents[virus_str][0]
    for player in game.players:
        player.remaining_move_chunks =\
            Player.CHUNKS_IN_MOVE if player.virus is virus else 0
    db.session.commit()


class TestCaseWithAppContext(unittest.TestCase):
    def __call__(self, *args, **kwargs):
        with app.app_context():
            return super().__call__(*args, **kwargs)


class TestCaseUsedGame(TestCaseWithAppContext):
    def __init__(self, *args, **kwargs):
        self.games: List[Game] = []
        super().__init__(*args, **kwargs)

    def create_game(self, row_count: int = 10,
                    col_count: int = 10, players_number: int = 2) -> Game:
        game = game_create(row_count, col_count, players_number)
        self.games.append(game)
        return game

    def tearDown(self):
        for game in self.games:
            game_remove(game)
