import unittest
from collections import namedtuple
from typing import List

from backend.models import Game, User, Player
from backend.tests.utils import (TestCaseUsedGame, cell_contents,
                                 set_active_player, set_board, viruses)


class TestGameMethods(TestCaseUsedGame):
    def test_make_chunk_of_move_error(self):
        game = self.create_game()
        set_board([
            ['_', '_', '_', '_', '_', 'x', '_', '_', 'o', 'o'],    # 10
            ['o', 'o', 'o', '_', '☒', 'o', '☒', '_', 'o', '_'],    # 9
            ['_', '_', '☒', 'o', 'o', '☒', '_', '●', '●', '_'],   # 8
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 7
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 6
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 5
            ['x', '_', '_', 'o', '●', '●', 'x', '_', '_', '☒'],    # 4
            ['☒', '☒', '☒', '_', 'o', '_', '_', 'x', 'x', '_'],    # 3
            ['_', 'x', '_', '_', '_', '_', '_', '_', '_', 'x'],    # 2
            ['x', '_', '_', '_', '_', '_', '_', '_', 'o', 'o'],    # 1
            # 1    2    3    4    5    6    7    8    9    10
        ], game)
        set_active_player(game, 'x')

        TestCase = namedtuple("TestCase", ("row", "col"))
        test_cases: List[TestCase] = [
            TestCase(row=10, col=20),
            TestCase(row=-1, col=10),
            TestCase(row=0, col=10),
            TestCase(row=1, col=1),
            TestCase(row=9, col=3),
            TestCase(row=4, col=6),
            TestCase(row=4, col=10),
            TestCase(row=6, col=5),
        ]

        for case in test_cases:
            with self.subTest(row=case.row, col=case.col):
                with self.assertRaises(ValueError):
                    game.make_chunk_of_move(case.row-1, case.col-1, False)

    def test_make_chunk_of_move(self):
        game = self.create_game()
        set_board([
            ['_', '_', '_', '_', '_', 'x', '_', '_', 'o', 'o'],    # 10
            ['o', 'o', 'o', '_', '☒', 'o', '☒', '_', 'o', '_'],    # 9
            ['_', '_', '☒', 'o', 'o', '☒', '_', '●', '●', '_'],   # 8
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 7
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 6
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 5
            ['x', '_', '_', 'o', '●', '●', 'x', '_', '_', '☒'],    # 4
            ['☒', '☒', '☒', '_', 'o', '_', '_', 'x', 'x', '_'],    # 3
            ['_', 'x', '_', '_', '_', '_', '_', '_', '_', 'x'],    # 2
            ['x', '_', '_', '_', '_', '_', '_', '_', 'o', 'o'],    # 1
            # 1    2    3    4    5    6    7    8    9    10
        ], game)
        set_active_player(game, 'x')
        owners = {}
        for player in game.players:
            owners[player.virus] = player.id

        TestCase = namedtuple(
            "TestCase", ("row", "col", "content")
        )
        test_cases: List[TestCase] = [
            TestCase(row=4, col=4, content='☒'),
            TestCase(row=3, col=5, content='☒'),
            TestCase(row=10, col=7, content='x'),
            TestCase(row=10, col=6, content='●'),
            TestCase(row=10, col=7, content='●'),
            TestCase(row=2, col=8, content='o'),
        ]

        for case in test_cases:
            with self.subTest(row=case.row, col=case.col):
                game.make_chunk_of_move(case.row-1, case.col-1)
                cell = game.get_cell(case.row-1, case.col-1)
                self.assertTrue(
                    cell.is_captured == cell_contents[case.content][1] and
                    cell.owner.id == owners[cell_contents[case.content][0]]
                )

    def test_join_user(self):
        total_players_counts: List[int] = [2, 3, 4]
        for players_count in total_players_counts:
            game = Game(total_players_number=players_count)
            users = []
            for index in range(players_count):
                user = User(str(index)*10, str(index)*10)
                user.id = index
                users.append(user)
            for i, user in enumerate(users):
                game.join_user(
                    user, viruses[i], Player.VIRUS_COLORS[i], commit=False
                )
                with self.subTest(
                    type="players count",
                    total_players_count=players_count,
                    added_player_order=i+1,
                ):
                    self.assertEqual(len(game.players), i + 1)
                with self.subTest(
                    type="available colors count",
                    total_players_count=players_count,
                    added_player_order=i+1,
                ):
                    self.assertEqual(
                        len(game.available_colors()),
                        len(Player.VIRUS_COLORS) - i - 1
                    )


if __name__ == "__main__":
    unittest.main()
