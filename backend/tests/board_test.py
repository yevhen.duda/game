import unittest
from collections import namedtuple
from typing import Callable, List

from backend.tests.utils import cell_contents, create_board, empty_board


class TestBoardMixinMethods(unittest.TestCase):
    def test_neighbors_indexes(self):
        TestCase = namedtuple(
            "TestCase", ("cell_index", "expected_neighbors")
        )
        test_cases: List[TestCase] = [
            TestCase(cell_index=0, expected_neighbors=3),
            TestCase(cell_index=1, expected_neighbors=5),
            TestCase(cell_index=15, expected_neighbors=8),
        ]
        board = empty_board()

        for case in test_cases:
            with self.subTest(
                expected_neighbors=case.expected_neighbors,
                cell_index=case.cell_index
            ):
                self.assertEqual(
                    case.expected_neighbors,
                    len(board.get_neighbors_coordinates(
                        board._cells[case.cell_index]
                    ))
                )

    def test_is_neighbors_make_cell_accessible(self):
        board = create_board([
            ['_', '_', '_', '_', '_', 'x', '_', '_', 'o', 'o'],    # 10
            ['o', 'o', '_', '_', '☒', '_', '_', '_', 'o', '_'],    # 9
            ['_', '_', '☒', '_', '_', '☒', '_', '●', '●', '_'],   # 8
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 7
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 6
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', '_'],    # 5
            ['x', '_', '_', 'o', '●', '●', 'x', '_', '_', '☒'],   # 4
            ['☒', 'o', 'o', '_', 'o', '_', '_', 'x', 'x', '_'],    # 3
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', 'x'],    # 2
            ['x', '_', '_', '_', '_', '_', '_', '_', 'o', 'o'],    # 1
            # 1    2    3    4    5    6    7    8    9    10
        ])

        TestCase = namedtuple("TestCase", ("virus", "indexes", "result"))
        cell_indexes = namedtuple("CellIndexes", ("row", "col"))
        test_cases: List[TestCase] = [
            TestCase(virus='x', indexes=cell_indexes(4, 2), result=True),
            TestCase(virus='o', indexes=cell_indexes(5, 7), result=True),
            TestCase(virus='o', indexes=cell_indexes(5, 8), result=False),
            TestCase(virus='x', indexes=cell_indexes(5, 4), result=False),
            TestCase(virus='o', indexes=cell_indexes(10, 4), result=False),
        ]

        for case in test_cases:
            with self.subTest(virus=case.virus, indexes=case.indexes):
                assertType: Callable =\
                    self.assertTrue if case.result else self.assertFalse
                assertType(board.is_neighbors_make_cell_accessible(
                    cell_contents[case.virus][0],
                    board.get_cell(case.indexes.row-1, case.indexes.col-1),
                    set()
                ))

    def test_is_permitted_chunk_of_move(self):
        board = create_board([
            ['_', '_', '_', '_', '_', '_', '_', '_', '_', 'o'],    # 10
            ['_', '_', '_', 'x', '_', '_', '_', '_', '_', 'o'],    # 9
            ['_', '_', 'x', '_', '_', '_', '_', '_', 'o', '_'],    # 8
            ['_', 'x', '_', '_', 'x', '_', '☒', '_', 'o', '_'],    # 7
            ['x', '_', '_', '_', 'x', '_', '●', 'o', '_', 'o'],    # 6
            ['x', '_', '_', 'x', '_', '●', '_', 'o', '_', 'o'],    # 5
            ['_', 'x', '_', 'x', '●', 'o', '_', 'o', 'o', 'o'],    # 4
            ['_', '_', 'x', '_', '_', '_', 'o', '_', '_', '_'],    # 3
            ['_', 'x', '_', '_', '_', '_', '_', 'o', '_', '_'],    # 2
            ['x', '_', 'x', '_', '_', '_', '_', '_', '_', '_'],    # 1
            # 1    2    3    4    5    6    7    8    9    10
        ])

        TestCase = namedtuple("TestCase", ("virus", "indexes", "result"))
        cell_indexes = namedtuple("CellIndexes", ("row", "col"))
        test_cases: List[TestCase] = [
            TestCase(virus='x', indexes=cell_indexes(5, 10), result=False),
            TestCase(virus='o', indexes=cell_indexes(1, 1), result=False),
            TestCase(virus='x', indexes=cell_indexes(4, 5), result=False),
            TestCase(virus='o', indexes=cell_indexes(8, 6), result=False),
            TestCase(virus='x', indexes=cell_indexes(8, 6), result=True),
            TestCase(virus='x', indexes=cell_indexes(5, 5), result=True),
            TestCase(virus='x', indexes=cell_indexes(4, 6), result=False),
            TestCase(virus='o', indexes=cell_indexes(5, 5), result=True),
        ]

        for case in test_cases:
            with self.subTest(virus=case.virus, indexes=case.indexes):
                assertType: Callable =\
                    self.assertTrue if case.result else self.assertFalse
                assertType(board.is_permitted_chunk_of_move(
                    cell_contents[case.virus][0],
                    case.indexes.row-1,
                    case.indexes.col-1
                ))

    def test_possible_moves(self):
        board = create_board([
            ['_', '_', '_', 'o'],    # 6
            ['_', 'x', '_', 'o'],    # 5
            ['x', '_', '_', 'o'],    # 4
            ['☒', 'o', 'o', '_'],    # 3
            ['x', '_', '_', '_'],    # 2
            ['x', '_', '_', '_'],    # 1
            # 1    2    3    4
        ])

        TestCase = namedtuple(
            "TestCase", ("virus", "indexes", "result")
        )
        cell_indexes = namedtuple("CellIndexes", ("row", "col"))
        test_cases: List[TestCase] = [
            TestCase(virus='x', indexes=cell_indexes(1, 1), result=False),
            TestCase(virus='o', indexes=cell_indexes(1, 1), result=False),
            TestCase(virus='o', indexes=cell_indexes(3, 4), result=True),
            TestCase(virus='o', indexes=cell_indexes(4, 1), result=True),
            TestCase(virus='x', indexes=cell_indexes(3, 2), result=True),
            TestCase(virus='x', indexes=cell_indexes(3, 3), result=False),
        ]
        possible_moves_count = {
            'o': 10,
            'x': 10,
        }

        for case in test_cases:
            with self.subTest(virus=case.virus, indexes=case.indexes):
                assertType: Callable =\
                    self.assertTrue if case.result else self.assertFalse
                assertType(
                    (case.indexes.row-1, case.indexes.col-1,) in
                    board.possible_moves(cell_contents[case.virus][0])
                )

        for virus, count in possible_moves_count.items():
            with self.subTest(virus=virus):
                self.assertEqual(
                    len(board.possible_moves(cell_contents[case.virus][0])),
                    count
                )


if __name__ == "__main__":
    unittest.main()
