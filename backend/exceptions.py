from typing import Optional


class ViewException(Exception):
    def __init__(self, message: str):
        self.message = message


class APIException(ViewException):
    def __init__(self, message: str, code: int = 400):
        self.code = code
        super().__init__(message)


class SocketException(ViewException):
    def __init__(
        self,
        message: Optional[str] = None,
        *rooms
    ) -> None:
        self.rooms = [*rooms] if len(rooms) > 0 else None
        super().__init__(message)
