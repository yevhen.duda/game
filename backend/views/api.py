from flask import Blueprint, request
from flask_jwt_extended import current_user
from flask_restful import Api, Resource, reqparse
from sqlalchemy import func

from backend.enums import NotificationType
from backend.exceptions import APIException
from backend.models import Game, Player, User, db
from backend.representations import (game_representation,
                                     game_short_representation)
from backend.utils.celery import run_move_time_inspector
from backend.utils.queries import game_statistic
from backend.utils.request_parser import (JoinGameNamespace, RequestArgument,
                                          board_height_argument_kwargs,
                                          board_width_argument_kwargs,
                                          color_argument_kwargs,
                                          player_number_argument_kwargs,
                                          virus_argument_kwargs)
from backend.utils.socket import send_game, send_notification, send_open_games

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

auth_parser = reqparse.RequestParser(argument_class=RequestArgument)
auth_parser.add_argument("username")
auth_parser.add_argument("password")


class UserAPI(Resource):
    def post(self):
        args = auth_parser.parse_args()
        if User.query.filter_by(username=args["username"]).one_or_none():
            raise APIException("This username already exists. "
                               "Please, change username", 401)
        try:
            new_user = User(
                username=args["username"],
                password=args["password"]
            )
        except ValueError as exc:
            raise APIException(exc.args[0], 401)
        db.session.add(new_user)
        db.session.commit()
        token = new_user.generate_auth_token()
        return {"access_token": token}, 200


class AuthTokenAPI(Resource):
    def post(self):
        args = auth_parser.parse_args()
        user = User.query.filter_by(username=args["username"]).one_or_none()
        if not user or not user.check_password(args["password"]):
            raise APIException("Invalid username or password", 401)
        token = user.generate_auth_token()
        return {"access_token": token}, 200


class StatisticAPI(Resource):
    def get(self):
        try:
            statistic = game_statistic(
                current_user,
                int(request.args.get('page', 1)) - 1,
            )
        except ValueError:
            raise APIException("Incorrect page index")
        return statistic, 200


game_create_parser = reqparse.RequestParser(argument_class=RequestArgument)
game_create_parser.add_argument("board_height", **board_height_argument_kwargs)
game_create_parser.add_argument("board_width", **board_width_argument_kwargs)
game_create_parser.add_argument("virus", **virus_argument_kwargs)
game_create_parser.add_argument("color", **color_argument_kwargs)
game_create_parser.add_argument(
    "players_number", **player_number_argument_kwargs
)


join_to_game_parser = reqparse.RequestParser(
    argument_class=RequestArgument, namespace_class=JoinGameNamespace
)
join_to_game_parser.add_argument("game_id", type=int)
join_to_game_parser.add_argument("virus", **virus_argument_kwargs)
join_to_game_parser.add_argument("color", **color_argument_kwargs)


class OpenedGamesAPI(Resource):
    """
    API for create games those expect second user.
    """
    @staticmethod
    def get_opened_games():
        return Game.query.outerjoin(Player).\
            group_by(Game.id).\
            having(func.count(Player.id) < Game.total_players_number)

    def get(self):
        return [
            game_short_representation(game) for game
            in OpenedGamesAPI.get_opened_games()
        ], 200

    def post(self):
        """
        Create a new Game.
        """
        args = game_create_parser.parse_args()
        new_game = Game(
            args["board_height"],
            args["board_width"],
            total_players_number=args["players_number"],
        )
        new_game.join_user(current_user, args["virus"], args["color"])
        db.session.add(new_game)
        db.session.commit()
        send_open_games(OpenedGamesAPI.get_opened_games())
        return game_representation(new_game), 200

    def put(self):
        """
        Join to game.
        """
        args = join_to_game_parser.parse_args()
        game = args["game"]

        game.join_user(current_user, args["virus"], args["color"])
        if game.is_active:
            run_move_time_inspector(game)

        send_open_games(OpenedGamesAPI.get_opened_games())
        send_notification(
            message='Opponent joined to your game'
                    if not game.is_players_complete else
                    'All opponents joined to your game',
            recepients=[player.user.id for player in game.players
                        if player.user.id != current_user.id],
            type=NotificationType.SUCCESS
        )
        send_game(game, [player.user.id for player in game.players])
        return game_representation(game), 200


api.add_resource(UserAPI, '/auth/registration', endpoint='registration')
api.add_resource(AuthTokenAPI, '/auth/token', endpoint='login')
api.add_resource(StatisticAPI, '/statistic')
api.add_resource(OpenedGamesAPI, '/game/opened')
