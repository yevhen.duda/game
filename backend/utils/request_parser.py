from flask_restful import reqparse

from backend.enums import Virus
from backend.exceptions import APIException
from backend.models import Game, Player


def virus_type(value):
    return Virus(int(value))


virus_argument_kwargs = {
    "type": virus_type,
    "choices": list(Virus),
    "help": "You can't pick this virus"
}


color_argument_kwargs = {
    "choices": Player.VIRUS_COLORS,
    "help": "You can't pick this color"
}


board_height_argument_kwargs = {
    "type": int,
    "choices": list(
        range(Game.MINIMUM_BOARD_HEIGHT, Game.MAXIMUM_BOARD_HEIGHT+1)
    ),
    "help": f"height of board must be between {Game.MINIMUM_BOARD_HEIGHT} and "
            f"{Game.MAXIMUM_BOARD_HEIGHT}."
}


board_width_argument_kwargs = {
    "type": int,
    "choices": list(
        range(Game.MINIMUM_BOARD_WIDTH, Game.MAXIMUM_BOARD_WIDTH+1)
    ),
    "help": f"width of board must be between {Game.MINIMUM_BOARD_WIDTH} and "
            f"{Game.MAXIMUM_BOARD_WIDTH}."
}


player_number_argument_kwargs = {
    "type": int,
    "choices": list(
        range(Game.MINIMUM_PLAYERS_NUMBER, Game.MAXIMUM_PLAYERS_NUMBER+1)
    ),
    "help": f"number of players must be between {Game.MINIMUM_PLAYERS_NUMBER} "
            f"and {Game.MAXIMUM_PLAYERS_NUMBER}."
}


class RequestArgument(reqparse.Argument):
    def handle_validation_error(self, error, bundle_errors):
        raise APIException(
            self.help if self.help else f"Incorrect value of {self.name}"
        )


class JoinGameNamespace(reqparse.Namespace):
    def __init__(self, *args, **kwargs):
        self._is_cheked_arguments = False
        super().__init__(*args, **kwargs)

    def _get(self, name):
        return super().__getitem__(name)

    def __getitem__(self, name):
        if not self._get("_is_cheked_arguments"):
            self._set_game()
            self._check_arguments()
        return self._get(name)

    def __setitem__(self, name, value):
        if name != "_is_cheked_arguments":
            super().__setitem__("_is_cheked_arguments", False)
        return super().__setitem__(name, value)

    def _set_game(self):
        game = Game.query.get(self._get("game_id"))
        if not game:
            raise APIException(
                "You cannot join this game. "
                "Please, choose another or create new game."
            )
        self["game"] = game

    def _check_arguments(self):
        self._check_color()
        self._check_virus()
        self["_is_cheked_arguments"] = True

    def _check_color(self):
        if self._get("color") in [
                pl.color for pl in self._get("game").players
        ]:
            raise APIException("You can't pick this color")

    def _check_virus(self):
        if self._get("virus") in [
                pl.virus for pl in self._get("game").players
        ]:
            raise APIException("You can't pick this virus")
