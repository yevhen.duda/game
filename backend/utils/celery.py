from backend.celery_tasks import check_delay
from backend.models import Game


def run_move_time_inspector(game: Game):
    check_delay.apply_async(
        (game.id, game.move_beginning_date_time.isoformat(),),
        countdown=Game.MOVE_DURATION.seconds,
    )
