from typing import List

from flask_socketio import SocketIO, emit

from backend.config import Config
from backend.enums import NotificationType, SocketEvent
from backend.models import Game, User
from backend.representations import (game_representation,
                                     game_short_representation)
from backend.utils.queries import game_statistic


def send_data(event, data, room=None,
              broadcast: bool = False, emit_to_queue: bool = False):
    args = (event, data,)
    kwargs = {"broadcast": broadcast, "namespace": "/"}
    if room is not None:
        kwargs["room"] = room

    if not emit_to_queue:
        emit(*args, **kwargs)
    else:
        socketio = SocketIO(message_queue=Config.CELERY_BROKER_URL)
        socketio.emit(*args, **kwargs)


def send_notification(
    message, recepients, type=NotificationType.ERROR,
    emit_to_queue: bool = False
):
    send_data(
        SocketEvent.NOTIFICATION,
        {
            "type": type,
            "message": message,
        },
        room=recepients,
        emit_to_queue=emit_to_queue,
    )


def send_game(game: Game, recepients, emit_to_queue: bool = False):
    send_data(
        SocketEvent.DOWNLOAD_GAME,
        {"game": game_representation(game)},
        room=recepients,
        emit_to_queue=emit_to_queue,
    )


def send_game_to_all_players(game: Game, emit_to_queue: bool = False):
    send_game(
        game,
        [player.user_id for player in game.players],
        emit_to_queue=emit_to_queue,
    )


def send_games(games: List[Game], recepients, emit_to_queue: bool = False):
    send_data(
        SocketEvent.DOWNLOAD_GAMES,
        {"games": [game_representation(game) for game in games]},
        room=recepients,
        emit_to_queue=emit_to_queue,
    )


def send_open_games(games: List[Game], emit_to_queue: bool = False):
    send_data(
        SocketEvent.DOWNLOAD_OPEN_GAMES,
        {"games": [game_short_representation(game) for game in games]},
        broadcast=True,
        emit_to_queue=emit_to_queue,
    )


def send_statistic_first_page(user: User):
    send_data(
        SocketEvent.DOWNLOAD_GAME_STATISTIC,
        {"statistic": game_statistic(user)},
        room=user.id,
    )


def send_statistic_to_players(game: Game):
    for player in game.players:
        send_statistic_first_page(player.user)
