from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate

from backend.celery_tasks import celery
from backend.config import Config
from backend.exceptions import APIException
from backend.middlewares.api import handle_api_errors, verify_jwt
from backend.models import bcrypt, db, jwt
from backend.views.api import api, api_bp
from backend.views.socket import socketio

app = Flask(__name__)
app.config.from_object(Config)
app.register_blueprint(api_bp, url_prefix='/api')
db.init_app(app)
migrate = Migrate(app, db, directory='migrations')
api.init_app(app)
bcrypt.init_app(app)
jwt.init_app(app)
cors = CORS(app)
socketio.init_app(app, message_queue=Config.CELERY_BROKER_URL)
celery.init_app(app)


@app.before_request
def before_request():
    verify_jwt()


@app.errorhandler(APIException)
def errorhandler(error: APIException):
    return handle_api_errors(error)
