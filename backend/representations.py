import datetime
from math import ceil
from typing import Dict, List, Optional, Union

from backend.enums import Virus
from backend.models import Cell, Game, Player


def datetime_representation(dt: Optional[datetime.datetime]) -> str:
    if dt is None:
        return ''
    return dt.isoformat(timespec="seconds") + 'Z'


def virus_representation(virus: Optional[Virus]) -> str:
    return str(virus.value) if virus is not None else ''


def cell_representation(cell: Cell) -> dict:
    return {
        "row": cell.row_index,
        "col": cell.col_index,
        "virus": virus_representation(cell.virus),
        "is_captured": cell.is_captured,
        "is_reachable_for_virus": cell.is_reachable_for_virus,
    }


def board_representation(game: Game) -> List[List[Dict[str, str]]]:
    rows: List[List[Dict[str, str]]] = []
    for row in range(game.board_height):
        rows.append([
            cell_representation(game.get_cell(row, column))
            for column in range(game.board_width)
        ])
    return rows


def player_short_representation(player: Player):
    return {
        "name": player.user.username,
        "virus": virus_representation(player.virus),
        "color": player.color,
    }


def player_representation(player: Player):
    result = player_short_representation(player)
    result.update({
        "remaining_move_chunks": player.remaining_move_chunks,
        "result": player.result if player.result else '',
    })
    return result


def game_representation(game: Game) -> dict:
    for player in game.players:
        game.set_reachability_cells_for_virus(player.virus)

    return {
        "id": game.id,
        "players_count": game.total_players_number,
        "players": [player_representation(player) for player in game.players],
        "datetime_limit_for_move": datetime_representation(
            game.get_datetime_limit_for_move()
        ),
        "can_pass_move": game.is_allowed_pass_move(),
        "board": board_representation(game),
        "is_active": game.is_players_complete and not game.is_ended,
        "is_ended": game.is_ended,
    }


def game_short_representation(game: Game) -> Dict[str, Union[str, int]]:
    return {
        "id": game.id,
        "players_count": game.total_players_number,
        "players": [
            player_short_representation(player) for player in game.players
        ],
        "board_height": game.board_height,
        "board_width": game.board_width,
    }


def ended_game_representation(game: Game):
    return {
        "id": game.id,
        "players": [
            player_representation(player) for player in game.players
        ],
        "datetime_end_game": datetime_representation(game.end_game_date_time)
    }


def statistic_page_representation(
    games: List[Game], page_index: int, games_count_in_page: int,
    total_games_count: int
):
    return {
        'games_count': total_games_count,
        'pages_count': ceil(total_games_count / games_count_in_page),
        'page': {
            'page_index': page_index+1,
            'games': [ended_game_representation(game) for game in games],
        }
    }
