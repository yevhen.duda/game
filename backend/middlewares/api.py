from flask import request
from flask_jwt_extended import verify_jwt_in_request

from backend.exceptions import APIException


def verify_jwt():
    if request.endpoint not in ('api.login', 'api.registration',):
        verify_jwt_in_request()


def handle_api_errors(error: APIException):
    return {"message":  error.message, }, error.code
