import datetime
import re
from itertools import product
from typing import Dict, List, Optional, Set, Tuple

from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from flask_jwt_extended import JWTManager, create_access_token
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import null, orm
from sqlalchemy.schema import CheckConstraint, UniqueConstraint

from backend.enums import Virus

db = SQLAlchemy()
jwt = JWTManager()
bcrypt = Bcrypt()


class Player(db.Model):
    VIRUS_COLORS: Tuple[str] = (
        "black", "red", "orange", "green", "purple"
    )
    CHUNKS_IN_MOVE: int = 3
    RESULTS: Tuple[str] = ("win", "lose", "draw")

    __table_args__ = (
        UniqueConstraint("game_id", "user_id", name='game_user_unq'),
        UniqueConstraint("game_id", "color", name='game_color_unq'),
        UniqueConstraint("game_id", "virus", name='game_virus_unq'),
        CheckConstraint(
            " OR ".join(
                [f"color='{color}'" for color in VIRUS_COLORS]
            ),
            name='color_check',
        ),
        CheckConstraint(
            " OR ".join(
                [f"result='{result}'" for result in RESULTS]
            ),
            name='result_check',
        ),
        CheckConstraint(
            "remaining_move_chunks >= 0 AND "
            f"remaining_move_chunks <= {CHUNKS_IN_MOVE}"
        ),
    )

    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.Integer, db.ForeignKey("game.id"), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    virus = db.Column(db.Enum(Virus), nullable=False)
    color = db.Column(db.String(10), nullable=False)
    remaining_move_chunks = db.Column(
        db.Integer, default=0, nullable=False
    )
    is_previous_move_passed = db.Column(
        db.Boolean, default=False, nullable=False
    )
    result = db.Column(db.String(4), nullable=True)
    user = db.relationship("User", foreign_keys=[user_id])

    @property
    def is_alive(self) -> bool:
        return self.result is None


class Cell(db.Model):
    __table_args__ = (
        UniqueConstraint("game_id", "row_index", "col_index"),
        CheckConstraint("row_index >= 0"),
        CheckConstraint("col_index >= 0"),
    )

    id = db.Column(db.Integer, primary_key=True)
    row_index = db.Column(db.SmallInteger, nullable=False)
    col_index = db.Column(db.SmallInteger, nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey("player.id"))
    is_captured = db.Column(db.Boolean, default=False, nullable=False)
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), nullable=False)
    owner = db.relationship("Player")
    is_reachable_for_virus: Dict[Virus, bool] = {}

    @property
    def virus(self) -> Optional[Virus]:
        return self.owner.virus if self.owner is not None else None

    @orm.reconstructor
    def _set_default_values_for_no_db_fields(self):
        self.is_reachable_for_virus = {}


class BoardMixin:
    def size(self) -> Tuple[int, int]:
        """
        :return: Tuple[height, width]
        """
        raise NotImplementedError(
            f"{self.__class__} not implement method size"
        )

    def get_cell(self, row: int, col: int) -> Cell:
        """
        Define Cell by row and col.
        :raises: :class:`ValueError`: if row or col is incorrect.
        """
        raise NotImplementedError(
            f"{self.__class__} not implement method get_cell"
        )

    def _get_start_positions(self) -> Dict[Virus, Cell]:
        raise NotImplementedError(
            f"{self.__class__} not implement method beginning_positions"
        )

    @property
    def beginning_positions(self) -> Dict[Virus, Cell]:
        return self._get_start_positions()

    def _all_coordinates(self):
        """
        Generate tuples of row and col indexes for all cells
        """
        height, width = self.size()
        for row in range(height):
            for col in range(width):
                yield row, col

    def _all_cells(self):
        for row, col in self._all_coordinates():
            yield self.get_cell(row, col)

    def possible_moves(self, virus: Virus) -> Set[Tuple[int, int]]:
        result = set()
        for row, col in self._all_coordinates():
            if self.is_permitted_chunk_of_move(virus, row, col):
                result.add((row, col,))
        return result

    def set_reachability_cells_for_virus(self, virus: Virus):
        possible_moves = self.possible_moves(virus)
        for row, col in self._all_coordinates():
            self.get_cell(row, col).is_reachable_for_virus[virus.value] =\
                (row, col,) in possible_moves

    def is_permitted_chunk_of_move(
        self, virus: Virus, row: int, col: int
    ) -> bool:
        cell = self.get_cell(row, col)
        if cell.is_captured or virus is cell.virus:
            return False
        return self.is_neighbors_make_cell_accessible(virus, cell, set()) or (
            self.beginning_positions[virus].owner is None and
            cell.id == self.beginning_positions[virus].id
        )

    def is_existed_allowed_move_chunk(self, virus: Virus) -> bool:
        return len(self.possible_moves(virus)) != 0

    def no_live_viruses(self, virus: Virus) -> bool:
        for cell in self._all_cells():
            if virus is cell.virus and not cell.is_captured:
                return False
        return True

    def is_neighbors_make_cell_accessible(
        self, virus: Virus, cell: Cell, checked: Set[Tuple[int, int]]
    ) -> bool:
        neighbor_list = self.get_neighbors_coordinates(
            self.get_cell(cell.row_index, cell.col_index)
        )
        for coords in neighbor_list:
            if coords in checked:
                continue
            checked.add(coords)
            neighbour = self.get_cell(*coords)
            if neighbour.virus is not virus:
                continue
            # neighbour.virus is virus
            if not neighbour.is_captured:
                return True

            if self.is_neighbors_make_cell_accessible(
                    virus, neighbour, checked
            ):
                return True
        return False

    def get_neighbors_coordinates(self, cell: Cell) -> List[Tuple[int, int]]:
        result = []
        height, width = self.size()
        indexes = (cell.row_index, cell.col_index,)
        # product below return all combinations of (row, col,) for which
        # absolute value of differences with indexes are
        # no great than 1 by row and col
        for c in product(*(range(n-1, n+2) for n in indexes)):
            if c != indexes and\
                    0 <= c[0] < height and 0 <= c[1] < width:
                result.append(c)
        return result


class Game(db.Model, BoardMixin):
    MOVE_DURATION: datetime.timedelta = datetime.timedelta(seconds=45)
    MINIMUM_PLAYERS_NUMBER = 2
    MAXIMUM_PLAYERS_NUMBER = 4
    MINIMUM_BOARD_WIDTH: int = 4
    MAXIMUM_BOARD_WIDTH: int = 30
    MINIMUM_BOARD_HEIGHT: int = MINIMUM_BOARD_WIDTH
    MAXIMUM_BOARD_HEIGHT: int = MAXIMUM_BOARD_WIDTH

    __table_args__ = (
        CheckConstraint("total_players_number BETWEEN 2 AND 4"),
        CheckConstraint(
            f"board_height BETWEEN {MINIMUM_BOARD_HEIGHT} AND "
            f"{MAXIMUM_BOARD_HEIGHT}",
            name="board_height_check",
        ),
    )

    id = db.Column(db.Integer, primary_key=True)
    total_players_number = db.Column(
        db.SmallInteger, default=2, nullable=False
    )
    move_beginning_date_time = db.Column(
        db.DateTime, default=datetime.datetime.utcnow, nullable=True
    )
    end_game_date_time = db.Column(db.DateTime)
    board_height = db.Column(db.SmallInteger, nullable=False, default=10)
    _cells = db.relationship(
        "Cell",
        backref="game",
        cascade="all, delete-orphan",
        lazy=False,
        innerjoin=True,
        order_by="[Cell.row_index, Cell.col_index]"
    )
    players = db.relationship(
        "Player",
        backref="game",
        cascade="all, delete-orphan",
        lazy=False,
        order_by="Player.id"
    )

    @property
    def board_width(self) -> int:
        return len(self._cells) // self.board_height

    @property
    def is_ended(self) -> bool:
        return self.end_game_date_time is not None

    @property
    def is_active(self) -> bool:
        return self.move_beginning_date_time is not None and not self.is_ended

    @property
    def active_player(self):
        """
        :return: user that can do moves
        """
        for player in self.players:
            if player.remaining_move_chunks > 0:
                return player
        raise ValueError('Active player is undefined')

    @property
    def is_players_complete(self) -> bool:
        return len(self.players) == self.total_players_number

    @property
    def living_players(self) -> List[Player]:
        return [player for player in self.players if player.is_alive]

    def __init__(
        self, board_height: int = 10, board_width: int = 10, *args, **kwargs
    ):
        if not (Game.MINIMUM_BOARD_HEIGHT < board_height
                < Game.MAXIMUM_BOARD_HEIGHT):
            raise ValueError("Incorrect value of height")
        if not (Game.MINIMUM_BOARD_WIDTH < board_width
                < Game.MAXIMUM_BOARD_WIDTH):
            raise ValueError("Incorrect value of width")

        super().__init__(*args, **kwargs)
        self.board_height = board_height
        # don't use self._all_coordinates() because _cells is not created
        for row in range(self.board_height):
            for col in range(board_width):
                self._cells.append(Cell(row_index=row, col_index=col))

    def size(self) -> Tuple[int, int]:
        return (self.board_height, self.board_width,)

    def get_cell(self, row: int, col: int) -> Cell:
        """
        Define Cell by row and col.
        :raises: :class:`ValueError`: if row or col is incorrect.
        """
        if not (0 <= row < self.board_height and 0 <= col < self.board_width):
            raise ValueError('Incorrect row or column index')
        return self._cells[row*self.board_width + col]

    def _get_start_positions(self) -> Dict[Virus, Cell]:
        viruses = [player.virus for player in self.players]
        positions = [
            self.get_cell(0, 0),
            self.get_cell(self.board_height-1, self.board_width-1),
            self.get_cell(self.board_height-1, 0),
            self.get_cell(0, self.board_width-1),
        ]
        return {virus: positions[i] for i, virus in enumerate(viruses)}

    def available_colors(self) -> List[str]:
        excluded_colors = {player.color for player in self.players}
        return list(
            filter(lambda x: x not in excluded_colors, Player.VIRUS_COLORS)
        )

    def actualize(self):
        if not self.is_ended and self.is_players_complete:
            self._pass_moves(self._get_passed_moves_count())

    def _pass_moves(self, moves_count: int):
        for _ in range(moves_count):
            if self.is_ended:
                return
            self.pass_move()

    def _get_passed_moves_count(self) -> int:
        delta = datetime.datetime.utcnow() - self.move_beginning_date_time
        return delta.seconds // Game.MOVE_DURATION.seconds

    def get_datetime_limit_for_move(self) -> datetime.datetime:
        return self.move_beginning_date_time + Game.MOVE_DURATION

    def join_user(self, user, virus: Virus, color: str, commit: bool = True):
        if self.is_players_complete:
            raise ValueError("You can't join this game")
        for player in self.players:
            if user.id == player.user_id:
                raise ValueError("You already joined this game")
        self.players.append(Player(
            game_id=self.id,
            user_id=user.id,
            virus=virus,
            color=color,
            remaining_move_chunks=(
                Player.CHUNKS_IN_MOVE if len(self.players) == 0 else 0
            )
        ))
        if len(self.players) == self.total_players_number:
            self.move_beginning_date_time = datetime.datetime.utcnow()
        if commit:
            db.session.commit()

    def is_allowed_pass_move(self) -> bool:
        return (
            self.active_player.remaining_move_chunks == Player.CHUNKS_IN_MOVE
            or not self.is_existed_allowed_move_chunk(self.active_player.virus)
        )

    def pass_move(self):
        """
        :return: True if passed else False
        """
        if not self.is_players_complete:
            raise ValueError("Game is not beginned")

        self._change_active_player()
        all_players_passed = True
        for player in self.living_players:
            if not player.is_previous_move_passed:
                all_players_passed = False
                break
        if all_players_passed:
            self._set_end_game()

    def make_chunk_of_move(self, row: int, col: int, commit: bool = True):
        """
        :raises: :class:`ValueError`: if chunk of move is incorrect.
        """
        if not self.is_players_complete:
            raise ValueError("Game is not beginned")
        if not self.is_permitted_chunk_of_move(
            self.active_player.virus, row, col
        ):
            raise ValueError("You can't move to this cell")

        cell = self.get_cell(row, col)
        cell.is_captured = cell.owner is not None
        cell.owner = self.active_player

        for player in self.players:
            beginning_position = self.beginning_positions[player.virus]
            if (
                player.id != self.active_player.id and
                beginning_position.virus is not None and
                self.no_live_viruses(player.virus)
            ):
                player.result = 'lose'
                if len(self.living_players) == 1:
                    self._set_end_game()

        if not self.is_ended:
            if self.active_player.remaining_move_chunks == 1:
                self._change_active_player()
            else:
                self.active_player.remaining_move_chunks -= 1
                self.active_player.is_previous_move_passed = False
        if commit:
            db.session.commit()

    def _change_active_player(self):
        living_players = self.living_players
        old_active_player_index = -1
        for index in range(len(living_players)):
            if living_players[index].remaining_move_chunks > 0:
                old_active_player_index = index
                break

        living_players[old_active_player_index].is_previous_move_passed = \
            living_players[old_active_player_index].remaining_move_chunks == \
            Player.CHUNKS_IN_MOVE
        living_players[old_active_player_index].remaining_move_chunks = 0

        new_index = (old_active_player_index + 1) % len(living_players)
        living_players[new_index].remaining_move_chunks = Player.CHUNKS_IN_MOVE
        self.move_beginning_date_time = datetime.datetime.utcnow()

    def _set_end_game(self):
        self.end_game_date_time = datetime.datetime.utcnow()
        if len(self.living_players) == 1:
            self.living_players[0].result = 'win'
        else:
            for player in self.living_players:
                player.result = 'draw'


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, username: str, password: str):
        User._validate_username(username)
        User._validate_password(password)
        self.set_password_hash(password)
        super().__init__(username=username)

    @staticmethod
    def _validate_username(username: str):
        if len(username) < 4:
            raise ValueError("Username must contain 5 or more characters.")
        if re.fullmatch(r"[\w.]+", username) is None:
            raise ValueError(
                "Not allowed characters in username. Allowed symbols: "
                "small and big letters, digits, point, underscore."
            )

    @staticmethod
    def _validate_password(password: str):
        if len(password) < 5 or len(password) > 100:
            raise ValueError("Password must contain 5-100 characters.")

    def set_password_hash(self, password: str):
        self.password = generate_password_hash(password).decode('utf-8')

    def check_password(self, password: str) -> bool:
        return check_password_hash(self.password, password)

    def generate_auth_token(self):
        return create_access_token(
            identity=self, expires_delta=datetime.timedelta(days=7)
        )

    def get_games(self):
        return Game.query.join(Player).filter(
            Game.end_game_date_time == null(), self.id == Player.user_id
        ).all()


@jwt.user_identity_loader
def user_identity_lookup(user: User):
    return user.id


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(id=identity).one_or_none()
