# Virus War

[_Rules of the game_](https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%B9%D0%BD%D0%B0_%D0%B2%D0%B8%D1%80%D1%83%D1%81%D0%BE%D0%B2)

### Backend

__Database:__ PostgreSQL.

__Message Broker:__ RabbitMQ.

__Python.__ 

__Modules__:
Flask, psycopg2, Flask-SQLAlchemy, Flask-Migrate, Flask-RESTful, Flask-Login, Flask-Bcrypt, Flask-Corse,
flask-jwt-extended, Flask-SocketIO, Celery

Environment variables can be placed to _.env_ files in root of project folder and "frontend" folder 
(see .env.example files).

### Frontend

React, Redux, React Redux, React Router Dom, Redux Thunk, Redux Saga, React Bootstrap, dotenv, dotenv-expand

### Using

#### Inside of Docker containers

Set values for variables in _.env.example_ folder and rename this folder to _.env_.
Variables DATABASE_HOST, DATABASE_PORT and CELERY_BROKER_URL use data from _docker-compose.yml_.
Do not change these variables. Otherwise change the _docker-compose.yml_ accordingly to these variables.

Rename file _.env.example_ in frontend folder to _.env_.

Run command: `docker-compose up`

#### Without Docker

Install RabbitMQ: `sudo apt-get install rabbitmq-server`

Open terminal in _backend_ folder. 
Create virtual environment (`python -m venv venv` as example).
Activate virtual environment and install python modules.

Create empty database in PostgreSQL. Set values for variables in _.env.example_ folder and rename this folder to _.env_.
Run command `flask db upgrade`.

Now you can run development backend server: `flask run`.

Open main directory and run celery worker: `celery -A backend.celery_tasks worker -l info`

Open terminal in _frontend_ folder.
Run command `npm install`.
Set values of environment variables in _.env.example_ and rename this folder to _.env_.
Now you can run development backend server: `npm run start`.
